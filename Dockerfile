FROM archlinux:base-devel

LABEL version="0.2.0"
LABEL description="kmap-gis-geotag project's CI/CD image"
LABEL maintainer="nhein@paradoxdev.com"

ARG PACKAGER="Nicholas R. Hein <nhein@paradoxdev.com>"

RUN echo "> Initializing pacman keyring" && \
	pacman-key --init && \
	pacman --sync --refresh && \
	pacman --sync --needed --noconfirm archlinux-keyring

RUN echo "> Installing project dependencies" && \
	pacman --sync --needed --noconfirm rust qgis cfitsio libheif poppler arrow mariadb mariadb-libs podofo python-gdal python-yaml

RUN echo "> Installing CI dependencies" && \
	pacman --sync --needed --noconfirm git git-cliff && \
	cargo install --root=/usr cargo2junit

RUN echo "> Upgrading packages" && \
	pacman --sync --sysupgrade --noconfirm && \
	echo "> Cleaning up package cache" && \
	yes | sudo pacman --sync --clean --clean

RUN echo "> Configuring build user" && \
	useradd build --system --shell /bin/bash --home-dir /var/cache/build --create-home && \
	echo "build ALL=(root) NOPASSWD: /usr/bin/pacman,/usr/sbin/pacman-key,/usr/bin/git" > /etc/sudoers.d/build && \
	git config --system safe.directory '*'

USER build
WORKDIR /var/cache/build

CMD ["/usr/bin/bash"]
