#! /usr/bin/env make -f

SHELL = /bin/sh

INSTALL = /usr/bin/install
INSTALL_FILES = ${INSTALL} -vDm 644
CARGO = /usr/bin/cargo --locked --verbose

DESTDIR ?=
CARCH ?= x86_64

prefix = /usr
libdir = ${prefix}/lib
datarootdir = ${prefix}/share

export CARGO_INCREMENTAL := 0

.PHONY: help
help:
	@echo 'TARGETS:'
	@echo '  help __________________ show this help message'
	@echo '  all ___________________ build the default target'
	@echo '  fetch _________________ fetch the project dependencies'
	@echo '  install _______________ install the project'
	@echo '  test __________________ execute unit/integration tests'
	@echo ''
	@echo 'OPTIONS:'
	@echo '  CARCH _________________ the architecture to use in the rust target [default: "x86_64"]'
	@echo '  DESTDIR _______________ directory for staged installs [default: (none)]'
	@echo '  prefix ________________ install prefix [default: "/usr"] '
	@echo '  libdir ________________ static data directory prefix [default: "$${prefix}/lib"]'
	@echo '  datarootdir ___________ read-only program data prefix [default: "$${prefix}/share"]'

.PHONY: fetch
fetch:
	$(CARGO) fetch --target "${CARCH}-unknown-linux-gnu" --locked --verbose

.PHONY: all
all: | fetch
	$(CARGO) build --release --locked --offline --verbose

.PHONY: test
test: | all
	$(CARGO) test --locked --offline --verbose

.PHONY: install
install: | all
	@echo "INSTALLING"

	@echo 'installing executable'
	$(CARGO) install --root ${DESTDIR}${prefix} --path . --no-track --verbose

	@echo 'installing default config file'
	$(INSTALL_FILES) -T dist/config.toml \
		${DESTDIR}${datarootdir}/factory/etc/kmap/gis/geotag.toml

	@echo 'installing systemd units'
	$(INSTALL_FILES) -t ${DESTDIR}${libdir}/systemd/system/ \
		dist/systemd/kmap-gis-geotag.service \
		dist/systemd/kmap-photo-sync@.service \
		dist/systemd/media@.mount \
		dist/systemd/media.automount

	@echo 'installing systemd presets'
	$(INSTALL_FILES) -t ${DESTDIR}${libdir}/systemd/system-preset/ \
		dist/systemd/40-kmap-gis-geotag.preset

	@echo 'installing udev rules'
	$(INSTALL_FILES) -t ${DESTDIR}${libdir}/udev/rules.d/ \
		dist/udev/10-sdcard-automount.rules

	@echo 'allocating sysusers'
	$(INSTALL_FILES) -T dist/kmapgisgeotag.sysusers \
		${DESTDIR}${libdir}/sysusers.d/kmapgisgeotag.conf

	@echo 'configuring tmpfiles'
	$(INSTALL_FILES) -T dist/kmapgisgeotag.tmpfiles \
		${DESTDIR}${libdir}/tmpfiles.d/kmapgisgeotag.conf
