# KMap GIS Geotag

Syncs photos from an SD card to an internal directory and generates a QGIS
geotagged layer.

## Contributing

All commits to this project must follow [the conventional commit format v1.0.0](https://www.conventionalcommits.org).

## Docker

Build the image:

```bash
docker build --tag kmap-gis-geotag .
```

Find the image:

```bash
docker image list | grep kmap-gis-geotag
```

Run the image:

```bash
docker run -it kmap-gis-geotag bash
```

Delete the image:

```bash
docker image rm kmap-gis-geotag
```
