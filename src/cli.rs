use clap::{Parser, ValueHint};
use std::path::PathBuf;

#[derive(Parser, Debug, Default)]
#[clap(
    name = "kmap-gis-geotag",
    author = "Nicholas R. Hein",
    version,
    about = "Create a QGIS layer from EXIF data of photos",
    long_about = None,
)]
pub struct Cli {
    #[clap(short, long, default_value_t = false)]
    /// Print verbose log messages (may be increased by defining 'KMAP_LOGLVL')
    pub verbose: bool,

    #[clap(short, long, value_name = "FILE", value_hint = ValueHint::FilePath)]
    /// Path to a configuration file
    pub config: Option<PathBuf>,

    #[clap(short, long, default_value_t = false)]
    /// Traverse the input directory recursively for photos
    pub recursive: bool,

    #[clap(short, long, value_name = "STR")]
    /// Specify the prefix for the vector layer files' names
    pub name: Option<String>,

    #[clap(short, long, value_name = "DIR", value_hint = ValueHint::FilePath)]
    /// Specify additional directories to search for images
    pub input: Vec<PathBuf>,

    #[clap(short, long, value_name = "DIR", value_hint = ValueHint::FilePath)]
    /// Specify the output directory for the vector layer
    pub output: Option<PathBuf>,
}
