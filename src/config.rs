use serde::Deserialize;
use std::path::PathBuf;

#[non_exhaustive]
#[derive(Debug, Default, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub exec: Exec,
    #[serde(default)]
    pub input: Input,
    #[serde(default)]
    pub output: Output,
}

#[non_exhaustive]
#[derive(Debug, Default, Deserialize)]
pub struct Exec {
    #[serde(default)]
    pub log: Option<String>,
}

#[non_exhaustive]
#[derive(Debug, Default, Deserialize)]
pub struct Input {
    #[serde(default)]
    pub paths: Vec<PathBuf>,
    #[serde(default)]
    pub recursive: bool,
}

#[non_exhaustive]
#[derive(Debug, Default, Deserialize)]
pub struct Output {
    #[serde(default)]
    pub env: Option<String>,
    #[serde(default)]
    pub path: Option<PathBuf>,
    #[serde(default)]
    pub name: Option<String>,
}
