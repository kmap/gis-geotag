use std::collections::HashMap;
use std::env;
use std::io::{Error, ErrorKind, Result};
use std::path::{Path, PathBuf};
use std::process::Command;

#[derive(Debug, Clone)]
pub struct Geotagger {
    input: PathBuf,
    output: Option<PathBuf>,
    recursive: bool,
    env: QgisProcessEnv,
}

#[derive(Debug, Clone)]
struct QgisProcessEnv {
    cache_dir: PathBuf,
    runtime_dir: PathBuf,
    qt_qpa_platform: Option<String>,
}

impl Geotagger {
    pub fn new(indir: impl AsRef<Path>) -> Self {
        let mut env = QgisProcessEnv::new();
        env.init();

        Self {
            input: PathBuf::from(indir.as_ref()),
            output: None,
            recursive: false,
            env,
        }
    }

    pub fn recurse(&mut self, recurse: bool) -> &mut Self {
        self.recursive = recurse;
        self
    }

    pub fn output(&mut self, dest: impl AsRef<Path>) -> &mut Self {
        self.output = Some(PathBuf::from(dest.as_ref()));
        self
    }

    pub fn cache_dir(&mut self, dir: impl AsRef<Path>) -> &mut Self {
        self.env.cache_dir = PathBuf::from(dir.as_ref());
        self
    }

    pub fn runtime_dir(&mut self, dir: impl AsRef<Path>) -> &mut Self {
        self.env.runtime_dir = PathBuf::from(dir.as_ref());
        self
    }

    pub fn init(&self) -> Result<Command> {
        let indir = &self.input;
        if !indir.exists() {
            return Err(Error::new(ErrorKind::NotFound, "input directory not found"));
        } else if !indir.is_dir() {
            return Err(Error::new(ErrorKind::NotFound, "input is not a directory"));
        }

        let outfile = self
            .output
            .as_ref()
            .ok_or_else(|| Error::new(ErrorKind::InvalidInput, "no output specified"))?; // TODO: use config
        if outfile.exists() {
            return Err(Error::new(
                ErrorKind::NotFound,
                "output already exists not found",
            ));
        }

        let mut cmd = Command::new("qgis_process");
        cmd.envs(self.env.clone());
        cmd.args(["-platform", "offscreen"])
            .args(["run", "native:importphotos"])
            .arg("--distance-units=degrees")
            .arg("--area_units=m2")
            .arg("--ellipsoid=NONE")
            .arg("--")
            .arg(format!(
                "RECURSIVE={}",
                self.recursive.then_some("true").unwrap_or("false")
            ))
            .arg(format!(
                "FOLDER={}",
                indir.to_str().map_or_else(
                    || Err(Error::new(
                        ErrorKind::InvalidInput,
                        "invalid input directory"
                    )),
                    |str| Ok(str)
                )?
            ))
            .arg(format!(
                "OUTPUT={}",
                outfile.to_str().ok_or_else(|| Error::new(
                    ErrorKind::InvalidInput,
                    "invalid output filename"
                ))?
            ));
        Ok(cmd)
    }
}

impl<P: AsRef<Path>> From<P> for Geotagger {
    fn from(path: P) -> Self {
        Self::new(path)
    }
}

impl QgisProcessEnv {
    fn init(&mut self) {}
}

impl QgisProcessEnv {
    pub const DEFAULT_CACHE_DIR: &'static str = "/var/cache/kmap/gis";
    pub const DEFAULT_RUNTIME_DIR: &'static str = "/run/kmap/gis";
    pub const DEFAULT_QT_QPA_PLATFORM: Option<&'static str> = Some("offscreen");

    fn new() -> Self {
        Self {
            cache_dir: PathBuf::from(
                env::var("CACHE_DIRECTORY")
                    .unwrap_or_else(|_| String::from(Self::DEFAULT_CACHE_DIR)),
            ),
            runtime_dir: PathBuf::from(
                env::var("RUNTIME_DIRECTORY")
                    .unwrap_or_else(|_| String::from(Self::DEFAULT_RUNTIME_DIR)),
            ),
            qt_qpa_platform: Self::DEFAULT_QT_QPA_PLATFORM.map(|platform| String::from(platform)),
        }
    }
}

impl IntoIterator for QgisProcessEnv {
    type Item = (&'static str, String);
    type IntoIter = std::collections::hash_map::IntoIter<&'static str, String>;

    fn into_iter(self) -> Self::IntoIter {
        let mut vars = HashMap::from([
            // HACK: this is a work-around because of qgis auth db weirdness
            (
                "HOME",
                self.cache_dir
                    .to_str()
                    .unwrap_or(Self::DEFAULT_CACHE_DIR)
                    .to_owned(),
            ),
            (
                "RUNTIME_DIRECTORY",
                self.runtime_dir
                    .to_str()
                    .unwrap_or(Self::DEFAULT_RUNTIME_DIR)
                    .to_owned(),
            ),
        ]);

        if let Some(platform) = self.qt_qpa_platform {
            vars.insert("QT_QPA_PLATFORM", platform);
        }
        vars.into_iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_matches::assert_matches;
    use std::{env, ffi::OsStr};

    #[test]
    fn new_sets_input() {
        static PATH: &str = "/foo/bar/baz.txt";
        let geotag = Geotagger::new(PATH);

        assert_eq!(
            PATH,
            geotag.input.to_string_lossy(),
            "input should be set to the given path"
        );
    }

    #[test]
    fn recurse_sets_field() {
        let mut geotag = Geotagger::new("./bar/baz.txt");
        assert_eq!(
            geotag.recursive, false,
            "recursive field default to being unset"
        );
        assert_eq!(
            geotag.recurse(true).recursive,
            true,
            "recursive field should be set"
        );
        assert_eq!(
            geotag.recurse(false).recursive,
            false,
            "recursive field should be changeable"
        );
    }

    #[test]
    fn output_sets_field() {
        let mut geotag = Geotagger::new("baz/");
        assert_eq!(geotag.output, None, "output should default to being unset");
        assert_eq!(
            geotag
                .output("foo")
                .output
                .as_ref()
                .unwrap()
                .to_string_lossy(),
            "foo",
            "output should be set"
        );
    }

    #[test]
    fn init_successful_for_valid_paths() {
        let geotag = Geotagger {
            input: PathBuf::from("/"),               // Root should always exist
            output: Some(PathBuf::from("/foo.shp")), // Random file probably doesn't exist
            recursive: false,
            env: QgisProcessEnv::new(),
        };

        if !geotag.input.exists() {
            panic!(
                "test conditions not met!  Directory '{}' does not exist!",
                geotag.input.to_string_lossy()
            );
        } else if geotag.output.as_ref().unwrap().exists() {
            panic!(
                "test conditions not met!  File '{}' exists!",
                geotag.input.to_string_lossy()
            );
        }

        let result = geotag.init();
        assert_matches!(result, Ok(_), "command should be returned for valid paths");
    }

    #[test]
    fn init_fails_for_invalid_input_directory() {
        let outfile = PathBuf::from("/foo.shp");
        if outfile.exists() {
            panic!(
                "test conditions not met!  File '{}' exists!",
                outfile.to_string_lossy()
            );
        }

        let result = Geotagger::new("$%^&!*#&").output(outfile).init();
        assert_matches!(
            result,
            Err(_),
            "command should not be returned for an invalid input directory"
        );
    }

    #[test]
    fn init_fails_for_existing_output_file() {
        let mut geotag = Geotagger {
            input: PathBuf::from("/"), // Root should always exist
            output: None,
            recursive: false,
            env: QgisProcessEnv::new(),
        };

        let outfile = env::current_dir() // Current directory should contain a file when testing
            .expect("failed to get current directory")
            .read_dir()
            .expect("failed to read the current directory")
            .next()
            .expect("current directory should not be empty while testing")
            .expect("IO error while reading the directory")
            .path();

        let result = geotag.output(outfile).init();
        assert_matches!(
            result,
            Err(_),
            "command should not be returned as it would overwrite files"
        );
    }

    #[test]
    fn init_returns_command_with_correct_args() {
        let geotag = Geotagger {
            input: PathBuf::from("/"),               // Root should always exist
            output: Some(PathBuf::from("/foo.shp")), // Random file probably doesn't exist
            recursive: false,
            env: QgisProcessEnv::new(),
        };

        if !geotag.input.exists() {
            panic!(
                "test conditions not met!  Directory '{}' does not exist!",
                geotag.input.to_string_lossy()
            );
        } else if geotag.output.as_ref().unwrap().exists() {
            panic!(
                "test conditions not met!  File '{}' exists!",
                geotag.input.to_string_lossy()
            );
        }

        let cmd = geotag.init().unwrap();
        let args: Vec<&OsStr> = cmd
            .get_args()
            .filter(|arg| arg.to_string_lossy().contains("/foo.shp"))
            .collect();
        assert_ne!(
            args.len(),
            0,
            "there should be an argument containing the given output file"
        );
    }
}
