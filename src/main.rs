use clap::Parser;
use kmap_gis_geotag::{Config, Geotagger};
use log::{debug, error, info, LevelFilter};
use std::collections::HashSet;
use std::io::{Error, ErrorKind, Result};
use std::path::PathBuf;
use std::process::ExitCode;
use std::{env, fs};
use toml;

mod cli;
use cli::Cli;

const FILTER_ENV: &str = "KMAP_LOGLVL";
const DEFAULT_LOGLVL: LevelFilter = LevelFilter::Warn;

const DEFAULT_CONFIG_FILE: &str = "/etc/kmap/gis/geotag.toml";
const DEFAULT_INPUT_DIR: &str = "/usr/share/factory/kmap/photos";
const DEFAULT_LAYER_NAME_PREFIX: &str = "geotags";
const DEFAULT_LAYER_NAME_EXT: &str = "shp";

fn main() -> ExitCode {
    run().map_or_else(
        |err| {
            error!("{}", err);
            ExitCode::FAILURE
        },
        |_| ExitCode::SUCCESS,
    )
}

// TODO: REFACTOR
fn run() -> Result<()> {
    const VERBOSE_LOGLVL: LevelFilter = if cfg!(debug_assertions) {
        LevelFilter::Trace
    } else {
        LevelFilter::Info
    };

    let cli = Cli::parse();
    let config = toml::from_str::<Config>(&fs::read_to_string(
        cli.config
            .clone()
            .unwrap_or_else(|| DEFAULT_CONFIG_FILE.into()),
    )?)
    .map_err(|e| {
        Error::new(
            ErrorKind::InvalidData,
            format!("failed to parse config file: {}", e),
        )
    })?;

    env_logger::Builder::new()
        .filter_level(
            cli.verbose
                .then_some(VERBOSE_LOGLVL)
                .unwrap_or(DEFAULT_LOGLVL),
        )
        .parse_env(FILTER_ENV)
        .init();

    info!("Geotagger process started!");
    debug!("LOGLEVEL: {}", log::max_level());
    debug!("CLI: {:?}", cli);
    debug!("CONFIG: {:?}", config);

    let recursive = cli.recursive || config.input.recursive;
    let mut inputs = cli
        .input
        .iter()
        .chain(config.input.paths.iter())
        .cloned()
        .collect::<HashSet<PathBuf>>();
    if inputs.is_empty() {
        inputs.insert(DEFAULT_INPUT_DIR.into());
    }

    let dest = {
        let mut dir = None;
        if let Some(envvar) = config.output.env {
            dir = env::var(envvar).map(|path| PathBuf::from(path)).ok();
        }
        dir.or(cli.output)
            .or(config.output.path)
            .ok_or_else(|| Error::new(ErrorKind::InvalidInput, "no output directory specified"))?
    }
    .join(
        cli.name
            .or(config.output.name)
            .unwrap_or_else(|| DEFAULT_LAYER_NAME_PREFIX.to_string())
            + DEFAULT_LAYER_NAME_EXT,
    );

    debug!(
        "inputs: [{}]",
        inputs
            .iter()
            .map(|path| path.to_string_lossy())
            .collect::<Vec<_>>()
            .join(", ")
    );
    debug!("output: {}", dest.to_string_lossy());
    debug!("recursive: {}", recursive);

    info!("generating geotag layer");
    for input in inputs.iter() {
        let output = Geotagger::new(PathBuf::from(input))
            .recurse(recursive)
            .output(&dest)
            .init()?
            .output()?;
        if output.status.success() {
            info!("geotag layer generated from: {}", input.to_string_lossy());
        } else {
            return Err(Error::new(
                ErrorKind::Other,
                format!(
                    "geotag layer generation failed ({}):\nSTDOUT:\n{}\nSTDERR:\n{}",
                    output.status,
                    String::from_utf8_lossy(&output.stdout),
                    String::from_utf8_lossy(&output.stderr)
                ),
            ));
        }
    }
    Ok(())
}
