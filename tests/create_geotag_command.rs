use kmap_gis_geotag::Geotagger;
use std::io::{ErrorKind, Result};
use tempfile::TempDir;

#[test]
fn create_geotag_command() -> Result<()> {
    let mkdir = |name: &str| {
        TempDir::new().expect(&format!("failed to create {} directory for testing", name))
    };

    let indir = mkdir("input");
    let outdir = mkdir("output");
    let cachedir = mkdir("cache");
    let runtimedir = mkdir("runtime");

    let outfile = outdir.path().join("geotags.shp");

    let mut cmd = Geotagger::new(indir.path()) // Generate layer with no photos
        .recurse(true)
        .output(&outfile)
        .cache_dir(cachedir.path())
        .runtime_dir(runtimedir.path())
        .init()?;
    match cmd.spawn() {
        Ok(mut child) => {
            let status = child.wait()?;
            assert!(
                status.success(),
                "command should be configured properly (failure: {})",
                status
            );
        }
        Err(err) => {
            if err.kind() == ErrorKind::NotFound {
                panic!("`qgis_process` command not found for testing");
            } else {
                assert!(
                    false,
                    "command should spawn successfully (failure: {})",
                    err
                );
            }
        }
    }
    assert!(outfile.exists(), "output file should be generated");
    Ok(())
}
